# chef-rails-stack

Chef cookbook for efimeral servers setup using ubuntu.

## Details

Utilizar chef-client con la opción de local mode o chef solo

A instalar(cookbook):

* RVM con Ruby 2.2.2
* bundler, rails gems
* Cliente de mysql
* Nginx web server
* Imagemagick
* Git
* Loggly client [setup](https://www.loggly.com/docs/rails-logs/)
* NodeJS
* SQLite
* Compiladores GCC
* Run a test to find what is missing! using the NF App. Use vagrant or virtual machine

## References

* https://launchschool.com/blog/chef-basics-for-rails-developers
* http://gettingstartedwithchef.com/first-steps-with-chef.html